package com.mycompany.restaurante;

import ConectionBD.Conection;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author JAGAPRI
 */
public class B004_Mesas_Restaurante {

    int B004_Mesas_Restaurante_Id_Mesa;
    String B004_Mesas_Restaurante_Ocupada;

    public B004_Mesas_Restaurante() {
    }

    public int getB004_Mesas_Restaurante_Id_Mesa() {
        return B004_Mesas_Restaurante_Id_Mesa;
    }

    public void setB004_Mesas_Restaurante_Id_Mesa(int B004_Mesas_Restaurante_Id_Mesa) {
        this.B004_Mesas_Restaurante_Id_Mesa = B004_Mesas_Restaurante_Id_Mesa;
    }

    public String getB004_Mesas_Restaurante_Ocupada() {
        return B004_Mesas_Restaurante_Ocupada;
    }

    public void setB004_Mesas_Restaurante_Ocupada(String B004_Mesas_Restaurante_Ocupada) {
        this.B004_Mesas_Restaurante_Ocupada = B004_Mesas_Restaurante_Ocupada;
    }

    @Override
    public String toString() {
        return "B004_Mesas_Restaurante{" + "B004_Mesas_Restaurante_Id_Mesa=" + B004_Mesas_Restaurante_Id_Mesa + ", B004_Mesas_Restaurante_Ocupada=" + B004_Mesas_Restaurante_Ocupada + '}';
    }
    // Guardar

    public void guardar() throws ClassNotFoundException, SQLException {
        //CRUD - C
        Conection con = new Conection(); //instancia
        String sql = "INSERT INTO B004_Mesas_Restaurante (B004_Mesas_Restaurante_Id_Mesa,B004_Mesas_Restaurante_Ocupada) VALUES(" + getB004_Mesas_Restaurante_Id_Mesa() + ",'" + getB004_Mesas_Restaurante_Ocupada() + "',)";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);
    }

    // Consultar
    public void consultar() throws ClassNotFoundException, SQLException {
        //CRUD -R 
        Conection con = new Conection();
        String sql = "SELECT B004_Mesas_Restaurante_Id_Mesa,B004_Mesas_Restaurante_Ocupada FROM B004_Mesas_Restaurante";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);
    }

    // Actualizar
    public void actualizar() throws ClassNotFoundException, SQLException {
        // CRUD -U
        Conection con = new Conection();
        String sql = "UPDATE B004_Mesas_Restaurante SET B004_Mesas_Restaurante_Ocupada = " + getB004_Mesas_Restaurante_Ocupada() + "WHERE B004_Mesas_Restaurante_Id_Mesa =" + getB004_Mesas_Restaurante_Id_Mesa() + ";";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);

    }

    // Borrar
    public void borrar() throws SQLException, ClassNotFoundException {
        //CRUD -D
        Conection con = new Conection();
        String sql = "DELETE FROM B004_Mesas_Restaurante WHERE B004_Mesas_Restaurante_Id_Mesa =" + getB004_Mesas_Restaurante_Id_Mesa() + ";";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);
    }

}
