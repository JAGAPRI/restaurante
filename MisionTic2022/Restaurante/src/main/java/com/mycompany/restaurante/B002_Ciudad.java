package com.mycompany.restaurante;

import ConectionBD.Conection;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class B002_Ciudad {

    int B002_Ciudad_Id;
    String B002_Ciudad_Descripcion;
    int B002_Ciudad_Id_Pais;

    public B002_Ciudad(int B002_Ciudad_Id, String B002_Ciudad_Descripcion, int B002_Ciudad_Id_Pais) {
        this.B002_Ciudad_Id = B002_Ciudad_Id;
        this.B002_Ciudad_Descripcion = B002_Ciudad_Descripcion;
        this.B002_Ciudad_Id_Pais = B002_Ciudad_Id_Pais;
    }

    B002_Ciudad() {

    }

    public int getB002_Ciudad_Id() {
        return B002_Ciudad_Id;
    }

    public void setB002_Ciudad_Id(int B002_Ciudad_Id) {
        this.B002_Ciudad_Id = B002_Ciudad_Id;
    }

    public String getB002_Ciudad_Descripcion() {
        return B002_Ciudad_Descripcion;
    }

    public void setB002_Ciudad_Descripcion(String B002_Ciudad_Descripcion) {
        this.B002_Ciudad_Descripcion = B002_Ciudad_Descripcion;
    }

    public int getB002_Ciudad_Id_Pais() {
        return B002_Ciudad_Id_Pais;
    }

    public void setB002_Ciudad_Id_Pais(int B002_Ciudad_Id_Pais) {
        this.B002_Ciudad_Id_Pais = B002_Ciudad_Id_Pais;
    }

    @Override
    public String toString() {
        return "B002_Ciudad{" + "B002_Ciudad_Id=" + B002_Ciudad_Id + ", B002_Ciudad_Descripcion=" + B002_Ciudad_Descripcion + ", B002_Ciudad_Id_Pais=" + B002_Ciudad_Id_Pais + '}';
    }
        // Guardar
    public void guardar() throws ClassNotFoundException, SQLException {
        //CRUD - C
        Conection con = new Conection(); //instancia
        String sql = "INSERT INTO B002_Ciudad (B002_Ciudad_Id,B002_Ciudad_Descripcion,B002_Ciudad_Id_Pais) VALUES(" + getB002_Ciudad_Id() + ",'" + getB002_Ciudad_Descripcion()+",'"+getB002_Ciudad_Id_Pais()+",')";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);
    }

    // Consultar
    public void consultar() throws ClassNotFoundException, SQLException {
        //CRUD -R 
        Conection con = new Conection();
        String sql = "SELECT B002_Ciudad_Id,B002_Ciudad_Descripcion,B002_Ciudad_Id_Pais FROM B002_Ciudad";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);
    }

    // Actualizar
    public void actualizar() throws ClassNotFoundException, SQLException {
        // CRUD -U
        Conection con = new Conection();
        String sql = "UPDATE B002_Ciudad SET B002_Ciudad_Descripcion = " + getB002_Ciudad_Descripcion() + "WHERE B002_Ciudad_Id =" + getB002_Ciudad_Id() + ";";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);

    }

    // Borrar
    public void borrar() throws SQLException, ClassNotFoundException {
        //CRUD -D
        Conection con = new Conection();
        String sql = "DELETE FROM B002_Ciudad WHERE B002_Ciudad_Id =" + getB002_Ciudad_Id() + ";";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);

    }

}
