package com.mycompany.restaurante;

import ConectionBD.Conection;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author JAGAPRI
 */
public class T003_Platos {

    int T003_Platos_Id;
    String T003_Platos_Descripcion;

    public T003_Platos() {
    }

    public int getT003_Platos_Id() {
        return T003_Platos_Id;
    }

    public void setT003_Platos_Id(int T003_Platos_Id) {
        this.T003_Platos_Id = T003_Platos_Id;
    }

    public String getT003_Platos_Descripcion() {
        return T003_Platos_Descripcion;
    }

    public void setT003_Platos_Descripcion(String T003_Platos_Descripcion) {
        this.T003_Platos_Descripcion = T003_Platos_Descripcion;
    }

    @Override
    public String toString() {
        return "T003_Platos{" + "T003_Platos_Id=" + T003_Platos_Id + ", T003_Platos_Descripcion=" + T003_Platos_Descripcion + '}';
    }

    // Guardar
    public void guardar() throws ClassNotFoundException, SQLException {
        //CRUD - C
        Conection con = new Conection(); //instancia
        String sql = "INSERT INTO T003_Platos (T003_Platos_id,T003_Platos_Descripcion) VALUES(" + getT003_Platos_Id() + ",'" + getT003_Platos_Descripcion() + "')";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);
    }

    // Consultar
    public void consultar() throws ClassNotFoundException, SQLException {
        //CRUD -R 
        Conection con = new Conection();
        String sql = "SELECT T003_Platos_id,T003_Platos_Descripcion FROM T003_Platos";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);
    }

    // Actualizar
    public void actualizar() throws ClassNotFoundException, SQLException {
        // CRUD -U
        Conection con = new Conection();
        String sql = "UPDATE T003_Platos SET T003_Platos_Descripcion = " + getT003_Platos_Descripcion() + "WHERE T003_Platos_id =" + getT003_Platos_Id() + ";";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);
    }

    // Borrar
    public void borrar() throws SQLException, ClassNotFoundException {
        //CRUD -D
        Conection con = new Conection();
        String sql = "DELETE FROM T003_Platos WHERE T003_Platos_id =" + getT003_Platos_Id() + ";";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);
    }
}
