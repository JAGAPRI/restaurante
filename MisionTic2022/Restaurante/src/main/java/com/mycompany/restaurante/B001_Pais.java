/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.restaurante;

import ConectionBD.Conection;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;


/**
 *
 * @author Delwi
 */
public class B001_Pais {
    int B001_Pais_Id;
    String B001_Pais_Descripcion;

    // Constructor
    public B001_Pais(int B001_Pais_Id, String B001_Pais_Descripcion) {
        this.B001_Pais_Id = B001_Pais_Id;
        this.B001_Pais_Descripcion = B001_Pais_Descripcion;
    }

    // Constructor vacio
    public B001_Pais() {
    }
    
    // Getter y Setter
    public int getB001_Pais_Id() {
        return B001_Pais_Id;
    }

    public void setB001_Pais_Id(int B001_Pais_Id) {
        this.B001_Pais_Id = B001_Pais_Id;
    }

    public String getB001_Pais_Descripcion() {
        return B001_Pais_Descripcion;
    }

    public void setB001_Pais_Descripcion(String B001_Pais_Descripcion) {
        this.B001_Pais_Descripcion = B001_Pais_Descripcion;
    }

    @Override
    public String toString() {
        return "B001_Pais{" + "B001_Pais_Id=" + B001_Pais_Id + ", B001_Pais_Descripcion=" + B001_Pais_Descripcion + '}';
    }
    
     // Guardar
    public void guardar() throws ClassNotFoundException, SQLException{
        //CRUD - C
        Conection con = new Conection(); //instancia
        String  sql = "INSERT INTO B001_Pais (B001_Pais_id,B001_Pais_Descripcion) VALUES("+getB001_Pais_Id()+",'"+getB001_Pais_Descripcion()+"')";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);
    }
    // Consultar
    public void consultar() throws ClassNotFoundException, SQLException{
        //CRUD -R 
        Conection con = new Conection();
        String  sql = "SELECT B001_Pais_Id,B001_Pais_Descripcion FROM B001_Pais";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);
    }
    
    // Actualizar
    
    public void actualizar() throws ClassNotFoundException, SQLException{
        // CRUD -U
        Conection con = new Conection();
        String sql = "UPDATE B001_Pais SET B001_Pais_Descripcion = "+getB001_Pais_Descripcion()+"WHERE B001_Pais_id ="+getB001_Pais_Id()+";";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);
    
    
    }
    
   
    // Borrar
   
    public void borrar() throws SQLException, ClassNotFoundException{
        //CRUD -D
        Conection con = new Conection();
        String sql = "DELETE FROM B001_Pais WHERE B001_Pis_id ="+getB001_Pais_Id()+";";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);
    
    }
    
    
    
    

            
}
