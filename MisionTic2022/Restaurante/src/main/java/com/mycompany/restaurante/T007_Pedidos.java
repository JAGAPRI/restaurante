/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.restaurante;

import java.sql.*;

/**
 *
 * @author Delwi
 */
public class T007_Pedidos {

    int T007_Pedidos_Id;
    int T007_Pedidoscol_Id_Cliente;
    Double T007_Pedidos_Precio;
    Boolean T007_Pedidos_Enviado;
    Date T007_Pedidos_Fecha_Pedido;
    Time T007_Pedidos_Hora_Entrega;
    int T007_Pedidos_Nro_Factura;
    int T007_Pedidos_Mesa;
    int T007_Pedidos_anulado;

    public T007_Pedidos(int T007_Pedidos_Id, int T007_Pedidoscol_Id_Cliente, Double T007_Pedidos_Precio, Boolean T007_Pedidos_Enviado, Date T007_Pedidos_Fecha_Pedido, Time T007_Pedidos_Hora_Entrega, int T007_Pedidos_Nro_Factura, int T007_Pedidos_Mesa, int T007_Pedidos_anulado) {
        this.T007_Pedidos_Id = T007_Pedidos_Id;
        this.T007_Pedidoscol_Id_Cliente = T007_Pedidoscol_Id_Cliente;
        this.T007_Pedidos_Precio = T007_Pedidos_Precio;
        this.T007_Pedidos_Enviado = T007_Pedidos_Enviado;
        this.T007_Pedidos_Fecha_Pedido = T007_Pedidos_Fecha_Pedido;
        this.T007_Pedidos_Hora_Entrega = T007_Pedidos_Hora_Entrega;
        this.T007_Pedidos_Nro_Factura = T007_Pedidos_Nro_Factura;
        this.T007_Pedidos_Mesa = T007_Pedidos_Mesa;
        this.T007_Pedidos_anulado = T007_Pedidos_anulado;
    }

    

    public T007_Pedidos() {
    }

    public int getT007_Pedidos_Id() {
        return T007_Pedidos_Id;
    }

    public void setT007_Pedidos_Id(int T007_Pedidos_Id) {
        this.T007_Pedidos_Id = T007_Pedidos_Id;
    }

    public int getT007_Pedidoscol_Id_Cliente() {
        return T007_Pedidoscol_Id_Cliente;
    }

    public void setT007_Pedidoscol_Id_Cliente(int T007_Pedidoscol_Id_Cliente) {
        this.T007_Pedidoscol_Id_Cliente = T007_Pedidoscol_Id_Cliente;
    }

    public Double getT007_Pedidos_Precio() {
        return T007_Pedidos_Precio;
    }

    public void setT007_Pedidos_Precio(Double T007_Pedidos_Precio) {
        this.T007_Pedidos_Precio = T007_Pedidos_Precio;
    }

    public Boolean getT007_Pedidos_Enviado() {
        return T007_Pedidos_Enviado;
    }

    public void setT007_Pedidos_Enviado(Boolean T007_Pedidos_Enviado) {
        this.T007_Pedidos_Enviado = T007_Pedidos_Enviado;
    }

    public Date getT007_Pedidos_Fecha_Pedido() {
        return T007_Pedidos_Fecha_Pedido;
    }

    public void setT007_Pedidos_Fecha_Pedido(Date T007_Pedidos_Fecha_Pedido) {
        this.T007_Pedidos_Fecha_Pedido = T007_Pedidos_Fecha_Pedido;
    }

    public Time getT007_Pedidos_Hora_Entrega() {
        return T007_Pedidos_Hora_Entrega;
    }

    public void setT007_Pedidos_Hora_Entrega(Time T007_Pedidos_Hora_Entrega) {
        this.T007_Pedidos_Hora_Entrega = T007_Pedidos_Hora_Entrega;
    }

    public int getT007_Pedidos_Nro_Factura() {
        return T007_Pedidos_Nro_Factura;
    }

    public void setT007_Pedidos_Nro_Factura(int T007_Pedidos_Nro_Factura) {
        this.T007_Pedidos_Nro_Factura = T007_Pedidos_Nro_Factura;
    }

    public int getT007_Pedidos_Mesa() {
        return T007_Pedidos_Mesa;
    }

    public void setT007_Pedidos_Mesa(int T007_Pedidos_Mesa) {
        this.T007_Pedidos_Mesa = T007_Pedidos_Mesa;
    }

    public int getT007_Pedidos_anulado() {
        return T007_Pedidos_anulado;
    }

    public void setT007_Pedidos_anulado(int T007_Pedidos_anulado) {
        this.T007_Pedidos_anulado = T007_Pedidos_anulado;
    }

    
    
}
