package com.mycompany.restaurante;

import ConectionBD.Conection;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.stream.Stream;

/**
 *
 * @author JAGAPRI
 */
public class T004_Bebidas {

    int T004_Bebidas_Id;
    String T004_Bebidas_Descripcion;
    double T004_Bebidas_Precio;
    Stream T004_Bebidas_Imagen;

    public T004_Bebidas(int T004_Bebidas_Id, Stream T004_Bebidas_Imagen) {
        this.T004_Bebidas_Id = T004_Bebidas_Id;
        this.T004_Bebidas_Imagen = T004_Bebidas_Imagen;
    }

    public int getT004_Bebidas_Id() {
        return T004_Bebidas_Id;
    }

    public void setT004_Bebidas_Id(int T004_Bebidas_Id) {
        this.T004_Bebidas_Id = T004_Bebidas_Id;
    }

    public String getT004_Bebidas_Descripcion() {
        return T004_Bebidas_Descripcion;
    }

    public void setT004_Bebidas_Descripcion(String T004_Bebidas_Descripcion) {
        this.T004_Bebidas_Descripcion = T004_Bebidas_Descripcion;
    }

    public double getT004_Bebidas_Precio() {
        return T004_Bebidas_Precio;
    }

    public void setT004_Bebidas_Precio(double T004_Bebidas_Precio) {
        this.T004_Bebidas_Precio = T004_Bebidas_Precio;
    }

    public Stream getT004_Bebidas_Imagen() {
        return T004_Bebidas_Imagen;
    }

    public void setT004_Bebidas_Imagen(Stream T004_Bebidas_Imagen) {
        this.T004_Bebidas_Imagen = T004_Bebidas_Imagen;
    }

    @Override
    public String toString() {
        return "T004_Bebidas{" + "T004_Bebidas_Id=" + T004_Bebidas_Id + ", T004_Bebidas_Descripcion=" + T004_Bebidas_Descripcion + ", T004_Bebidas_Precio=" + T004_Bebidas_Precio + ", T004_Bebidas_Imagen=" + T004_Bebidas_Imagen + '}';
    }

    // Guardar
    public void guardar() throws ClassNotFoundException, SQLException {
        //CRUD - C
        Conection con = new Conection(); //instancia
        String sql = "INSERT INTO T004_Bebidas (T004_Bebidas_id,T004_Bebidas_Descripcion,T004_Bebidas_Precio,T004_Bebidas_Imagen) VALUES(" + getT004_Bebidas_Id() + ",'" + getT004_Bebidas_Descripcion() + "','" + getT004_Bebidas_Precio() + "','" + getT004_Bebidas_Imagen() + "')";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);
    }

    // Consultar
    public void consultar() throws ClassNotFoundException, SQLException {
        //CRUD -R 
        Conection con = new Conection();
        String sql = "SELECT T004_Bebidas_id,T004_Bebidas_Descripcion,T004_Bebidas_Precio,T004_Bebidas_Imagen FROM T004_Bebidas";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);
    }

    // Actualizar
    public void actualizar() throws ClassNotFoundException, SQLException {
        // CRUD -U
        Conection con = new Conection();
        String sql = "UPDATE T004_Bebidas SET T004_Bebidas_Descripcion = " + getT004_Bebidas_Descripcion() + "WHERE T004_Bebidas_id =" + getT004_Bebidas_Id() + ";";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);

    }

    // Borrar
    public void borrar() throws SQLException, ClassNotFoundException {
        //CRUD -D
        Conection con = new Conection();
        String sql = "DELETE FROM T004_Bebidas WHERE T004_Bebidas_id =" + getT004_Bebidas_Id() + ";";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);

    }

}
