/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.restaurante;

import ConectionBD.Conection;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author JAGAPRI
 */
public class B003_Mesas {

    int B003_Mesas_Id;
    String B003_Mesas_Posicion;
    int B003_Mesas_Nro_Puestos;

    public B003_Mesas() {
    }

    public int getB003_Mesas_Id() {
        return B003_Mesas_Id;
    }

    public void setB003_Mesas_Id(int B003_Mesas_Id) {
        this.B003_Mesas_Id = B003_Mesas_Id;
    }

    public String getB003_Mesas_Posicion() {
        return B003_Mesas_Posicion;
    }

    public void setB003_Mesas_Posicion(String B003_Mesas_Posicion) {
        this.B003_Mesas_Posicion = B003_Mesas_Posicion;
    }

    public int getB003_Mesas_Nro_Puestos() {
        return B003_Mesas_Nro_Puestos;
    }

    public void setB003_Mesas_Nro_Puestos(int B003_Mesas_Nro_Puestos) {
        this.B003_Mesas_Nro_Puestos = B003_Mesas_Nro_Puestos;
    }

    @Override
    public String toString() {
        return "B003_Mesas{" + "B003_Mesas_Id=" + B003_Mesas_Id + ",B003_Mesas_Posicion=" + B003_Mesas_Posicion + ", B003_Mesas_Nro_Puestos=" + B003_Mesas_Nro_Puestos + '}';
    }

    // Guardar
    public void guardar() throws ClassNotFoundException, SQLException {
        //CRUD - C
        Conection con = new Conection(); //instancia
        String sql = "INSERT INTO B003_Mesas (B003_Mesas_id,B003_Mesas_Posicion,B003_Mesas_Nro_Puestos) VALUES(" + getB003_Mesas_Id() + ",'" + getB003_Mesas_Posicion() + "'," + getB003_Mesas_Nro_Puestos() + ")";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);
    }

    // Consultar
    public void consultar() throws ClassNotFoundException, SQLException {
        //CRUD -R 
        Conection con = new Conection();
        String sql = "SELECT B003_Mesas_id,B003_Mesas_Posicion,B003_Mesas_Nro_Puestos FROM B003_Mesas";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);
    }

    // Actualizar
    public void actualizar() throws ClassNotFoundException, SQLException {
        // CRUD -U
        Conection con = new Conection();
        String sql = "UPDATE B003_Mesas SET B003_Mesas_Posicion = " + getB003_Mesas_Posicion() + "WHERE B003_Mesas_id =" + getB003_Mesas_Id() + ";";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);

    }
    // Borrar

    public void borrar() throws SQLException, ClassNotFoundException {
        //CRUD -D
        Conection con = new Conection();
        String sql = "DELETE FROM B003_Mesas WHERE B00_Mesas_id =" + getB003_Mesas_Id() + ";";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);

    }
}
