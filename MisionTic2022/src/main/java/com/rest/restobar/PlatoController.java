package com.rest.restobar;

import com.rest.restobar.entity.Plato;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class PlatoController {


	@Autowired
	private PlatoService pservice;
	

	@RequestMapping("/plato")
	public String viewPlatos(Model model) {
		List<Plato> listPlatos = pservice.listAll();
		model.addAttribute("listPlatos", listPlatos);

		return "plato";

	}

	@RequestMapping("/newplato")
	public String showNewProductPage(Model model) {
		Plato plato = new Plato();
		model.addAttribute("plato", plato);
		
		return "new_plato";
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String savePlato(@ModelAttribute("plato") Plato plato) {
		pservice.save(plato);
		
		return "redirect:/plato";
	}
	
	@RequestMapping("/edit/{id}")
	public ModelAndView showEditplatoPage(@PathVariable(name = "id") int id) {
		ModelAndView mav = new ModelAndView("edit_plato");
		Plato plato = pservice.get(id);
		mav.addObject("plato", plato);
		
		return mav;
	}
	
	@RequestMapping("/delete/{id}")
	public String deletePlato(@PathVariable(name = "id") int id) {
		pservice.delete(id);
		return "redirect:/plato";
	}
}
