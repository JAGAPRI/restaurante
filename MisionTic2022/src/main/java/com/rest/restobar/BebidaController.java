package com.rest.restobar;

import com.rest.restobar.entity.Bebida;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class BebidaController {
    @Autowired
    private Bebidaservice bservice;

    @RequestMapping("/bebida")
    public String viewBebidas(Model modelb) {
        List<Bebida> listBebidas = bservice.listAll();
        modelb.addAttribute("listBebidas", listBebidas);

        return "bebida";

    }

    @RequestMapping("/newbebida")
    public String showNewBebidaPage(Model modelb) {
        Bebida bebida = new Bebida();
        modelb.addAttribute("bebida", bebida);

        return "new_bebida";
    }

    @RequestMapping(value = "/saveb", method = RequestMethod.POST)
    public String saveBebida(@ModelAttribute("bebida") Bebida bebida) {
        bservice.save(bebida);

        return "redirect:/bebida";
    }

    @RequestMapping("/editb/{id}")
    public ModelAndView showEditBebidaPage(@PathVariable(name = "id") int id) {
        ModelAndView mav = new ModelAndView("edit_bebida");
        Bebida bebida = bservice.get(id);
        mav.addObject("bebida", bebida);

        return mav;
    }

    @RequestMapping("/deleteb/{id}")
    public String deleteBebida(@PathVariable(name = "id") int id) {
        bservice.delete(id);
        return "redirect:/bebida";
    }
}
