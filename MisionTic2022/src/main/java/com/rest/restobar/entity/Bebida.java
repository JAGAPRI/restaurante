package com.rest.restobar.entity;

import javax.persistence.*;

@Table(name = "t004_bebidas")
@Entity
public class Bebida {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "T004_Bebidas_Id", nullable = false)
    private Integer id;

    @Column(name = "T004_Bebidas_Descripcion", length = 100)
    private String t004BebidasDescripcion;

    @Column(name = "T004_Bebidas_Precio")
    private Double t004BebidasPrecio;

    @Column(name = "T004_Bebidas_Imagen")
    private byte[] t004BebidasImagen;

    public Bebida() {
    }

    public Bebida(Integer id, String t004BebidasDescripcion, Double t004BebidasPrecio, byte[] t004BebidasImagen) {
        super();
        this.id = id;
        this.t004BebidasDescripcion = t004BebidasDescripcion;
        this.t004BebidasPrecio = t004BebidasPrecio;
        this.t004BebidasImagen = t004BebidasImagen;
    }

    public byte[] getT004BebidasImagen() {
        return t004BebidasImagen;
    }

    public void setT004BebidasImagen(byte[] t004BebidasImagen) {
        this.t004BebidasImagen = t004BebidasImagen;
    }

    public Double getT004BebidasPrecio() {
        return t004BebidasPrecio;
    }

    public void setT004BebidasPrecio(Double t004BebidasPrecio) {
        this.t004BebidasPrecio = t004BebidasPrecio;
    }

    public String getT004BebidasDescripcion() {
        return t004BebidasDescripcion;
    }

    public void setT004BebidasDescripcion(String t004BebidasDescripcion) {
        this.t004BebidasDescripcion = t004BebidasDescripcion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}