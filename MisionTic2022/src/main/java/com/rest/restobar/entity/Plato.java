package com.rest.restobar.entity;

import javax.persistence.*;

@Table(name = "t003_platos")
@Entity
public class Plato {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "T003_Platos_Id", nullable = false)
    private Integer id;

    @Column(name = "T003_Platos_Descripcion", length = 100)
    private String t003PlatosDescripcion;

    @Column(name = "T003_Platos_Precio")
    private Double t003PlatosPrecio;

    @Column(name = "T003_Platos_imagen")
    private byte[] t003Platosiimagen;

    @Column(name = "T003_Platos_ingredientes", length = 200)
    private String t003PlatosIngredientes;

    public Plato() {
    }

    public Plato(Integer id, String t003PlatosDescripcion, Double t003PlatosPrecio, byte[] t003Platosiimagen, String t003PlatosIngredientes) {
        super();
        this.id = id;
        this.t003PlatosDescripcion = t003PlatosDescripcion;
        this.t003PlatosPrecio = t003PlatosPrecio;
        this.t003Platosiimagen = t003Platosiimagen;
        this.t003PlatosIngredientes = t003PlatosIngredientes;
    }

    public String getT003PlatosIngredientes() {
        return t003PlatosIngredientes;
    }

    public void setT003PlatosIngredientes(String t003PlatosIngredientes) {
        this.t003PlatosIngredientes = t003PlatosIngredientes;
    }

    public byte[] getT003Platosiimagen() {
        return t003Platosiimagen;
    }

    public void setT003Platosiimagen(byte[] t003Platosiimagen) {
        this.t003Platosiimagen = t003Platosiimagen;
    }

    public Double getT003PlatosPrecio() {
        return t003PlatosPrecio;
    }

    public void setT003PlatosPrecio(Double t003PlatosPrecio) {
        this.t003PlatosPrecio = t003PlatosPrecio;
    }

    public String getT003PlatosDescripcion() {
        return t003PlatosDescripcion;
    }

    public void setT003PlatosDescripcion(String t003PlatosDescripcion) {
        this.t003PlatosDescripcion = t003PlatosDescripcion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}