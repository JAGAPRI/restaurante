package com.rest.restobar;

import com.rest.restobar.entity.Bebida;
import com.rest.restobar.repositorio.BebidaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional
public class Bebidaservice {
    @Autowired
    private BebidaRepository repob;

    public List<Bebida> listAll() {
        return repob.findAll();
    }

    public void save(Bebida Bebida) {
        repob.save(Bebida);
    }

    public Bebida get(Integer id) {

        return repob.findById(id).get();
    }

    public void delete(Integer id) {

        repob.deleteById(id);
    }
    
}
