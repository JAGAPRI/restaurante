package com.rest.restobar;

import com.rest.restobar.entity.Plato;
import com.rest.restobar.repositorio.PlatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class PlatoService {
    @Autowired
    private PlatoRepository repo;

    public List<Plato> listAll() {
        return repo.findAll();
    }

    public void save(Plato plato) {
        repo.save(plato);
    }

    public Plato get(Integer id) {
        return repo.findById(id).get();
    }

    public void delete(Integer id) {
        repo.deleteById(id);
    }
}
