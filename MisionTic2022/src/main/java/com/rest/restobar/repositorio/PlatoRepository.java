package com.rest.restobar.repositorio;

import com.rest.restobar.entity.Plato;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlatoRepository extends JpaRepository<Plato, Integer> {
}